
/ {
	fiq-debugger {
		status = "okay";
	};

	chosen {
		bootargs = "vmalloc=496M rockchip_jtag";
	};

	pwm_regulator1:pwm-regulator1 {
		compatible = "rockchip_pwm_regulator";
		pwms = <&pwm1 0 25000>;
		rockchip,pwm_id= <1>;
		rockchip,pwm_voltage_map= <950000 975000 1000000 1025000 1050000 1075000 1100000 1125000 1150000 1175000 1200000 1225000 1250000 1275000 1300000 1325000 1350000 1375000 1400000 1425000 1450000>;
		rockchip,pwm_voltage= <1250000>;
		rockchip,pwm_min_voltage= <950000>;
		rockchip,pwm_max_voltage= <1450000>;
		rockchip,pwm_suspend_voltage= <1250000>;
		rockchip,pwm_coefficient= <550>;
		status = "disabled";
		regulators {
			#address-cells = <1>;
			#size-cells = <0>;
			pwm_reg0: regulator@0 {
				regulator-compatible = "pwm_dcdc1";
				regulator-name= "vdd_arm";
				regulator-min-microvolt = <950000>;
				regulator-max-microvolt = <1450000>;
				regulator-always-on;
				regulator-boot-on;
			};
		};
	};

	pwm_regulator2:pwm-regulator2 {
		compatible = "rockchip_pwm_regulator";
		pwms = <&pwm2 0 25000>;
		rockchip,pwm_id= <2>;
		rockchip,pwm_voltage_map= <950000 975000 1000000 1025000 1050000 1075000 1100000 1125000 1150000 1175000 1200000 1225000 1250000 1275000 1300000 1325000 1350000 1375000 1400000 1425000 1450000>;
		rockchip,pwm_voltage= <1200000>;
		rockchip,pwm_min_voltage= <950000>;
		rockchip,pwm_max_voltage= <1450000>;
		rockchip,pwm_suspend_voltage= <1250000>;
		rockchip,pwm_coefficient= <550>;
		status = "disabled";
		regulators {
			#address-cells = <1>;
			#size-cells = <0>;
			pwm_reg1: regulator@1 {
				regulator-compatible = "pwm_dcdc2";
				regulator-name= "vdd_logic";
				regulator-min-microvolt = <950000>;
				regulator-max-microvolt = <1450000>;
				regulator-always-on;
				regulator-boot-on;
			};
		};
	};

};

&nandc {
	status = "disabled"; // used nand set "okay" ,used emmc set "disabled"
};

&nandc0reg {
	status = "okay"; // used nand set "disabled" ,used emmc set "okay"
};

&emmc {
	clock-frequency = <50000000>;
	clock-freq-min-max = <400000 50000000>;
	supports-highspeed;
	supports-emmc;
	bootpart-no-access;
	supports-DDR_MODE;
	ignore-pm-notify;
	keep-power-in-suspend;
	//poll-hw-reset
	status = "okay";
};

&sdmmc {
	clock-frequency = <37500000>;
	clock-freq-min-max = <400000 37500000>;
	supports-highspeed;
	supports-sd;
	broken-cd;
	card-detect-delay = <200>;
	ignore-pm-notify;
	keep-power-in-suspend;
	vmmc-supply = <&rk816_ldo6_reg>;
	status = "disabled";
};

&sdio {
	clock-frequency = <37500000>;
	clock-freq-min-max = <200000 37500000>;
	supports-highspeed;
	supports-sdio;
	ignore-pm-notify;
	keep-power-in-suspend;
	cap-sdio-irq;
	status = "okay";
};

&adc {
	status = "okay";

	key: key {
		compatible = "rockchip,key";
		io-channels = <&adc 1>;

		vol-up-key {
			linux,code = <115>;
			label = "volume up";
			rockchip,adc_value = <523>;
		};

		vol-down-key {
			linux,code = <114>;
			label = "volume down";
			rockchip,adc_value = <318>;
		};

		menu-key {
			linux,code = <59>;
			label = "menu";
			rockchip,adc_value = <146>;
		};

		back-key {
			linux,code = <158>;
			label = "back";
			rockchip,adc_value = <1>;
		};
	};
};

&i2c0 {
	status = "okay";
	rk816: rk816@1a {
		reg = <0x1a>;
		status = "okay";
	};
};

&i2c1 {
	status = "okay";
	ts@40 {
		compatible = "9tripod,gslX680";
        reg = <0x40>;
		touch-gpio = <&gpio3 GPIO_C5 IRQ_TYPE_EDGE_RISING>;
		reset-gpio = <&gpio0 GPIO_D1 GPIO_ACTIVE_LOW>;
        max-x = <1024>;
        max-y = <600>;
	};
    sensor@1d {
        compatible = "gs_mma8452";
        reg = <0x1d>;
        type = <SENSOR_TYPE_ACCEL>;
        irq-gpio = <&gpio0 GPIO_B4 IRQ_TYPE_EDGE_FALLING>;
        irq_enable = <1>;
        poll_delay_ms = <30>;
        layout = <1>;
	};
};

&i2c2 {
	status = "disabled";
};

&fb {
	rockchip,disp-mode = <ONE_DUAL>;
	rockchip,uboot-logo-on = <1>;
};

/include/ "rk816.dtsi"
&rk816 {
	gpios = <&gpio0 GPIO_B0 GPIO_ACTIVE_HIGH>, <&gpio3 GPIO_C1 GPIO_ACTIVE_LOW>;
	rk816,system-power-controller;
	rk816,support_dc_chg = <1>;/*1: dc chg; 0:usb chg*/
	io-channels = <&adc 0>;
	gpio-controller;
	#gpio-cells = <2>;

	regulators {
		rk816_dcdc1_reg: regulator@0 {
			regulator-name = "vdd_arm";
			regulator-min-microvolt = <700000>;
			regulator-max-microvolt = <1500000>;
			regulator-initial-mode = <0x2>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-mode = <0x2>;
				regulator-state-disabled;
				regulator-state-uv = <900000>;
			};
		};

		rk816_dcdc2_reg: regulator@1 {
			regulator-name = "vdd_logic";
			regulator-min-microvolt = <700000>;
			regulator-max-microvolt = <1500000>;
			regulator-initial-mode = <0x2>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-mode = <0x2>;
				regulator-state-enabled;
				regulator-state-uv = <1000000>;
			};
		};

		rk816_dcdc3_reg: regulator@2 {
			regulator-name = "rk816_dcdc3";
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <1200000>;
			regulator-initial-mode = <0x2>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-mode = <0x2>;
				regulator-state-enabled;
				regulator-state-uv = <1200000>;
			};
		};

		rk816_dcdc4_reg: regulator@3 {
			regulator-name = "vccio";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
			regulator-initial-mode = <0x1>;/*fast mode*/
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-mode = <0x2>;
				regulator-state-enabled;
				regulator-state-uv = <2800000>;
			};
		};

		rk816_ldo1_reg: regulator@4 {
			regulator-name = "rk816_ldo1";
			regulator-min-microvolt = <2800000>;
			regulator-max-microvolt = <2800000>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-disabled;
				regulator-state-uv = <2800000>;
			};
		};

		rk816_ldo2_reg: regulator@5 {
			regulator-name = "rk816_ldo2";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-disabled;
				regulator-state-uv = <1800000>;
			};
		};

		rk816_ldo3_reg: regulator@6 {
			regulator-name = "rk816_ldo3";
			regulator-min-microvolt = <1100000>;
			regulator-max-microvolt = <1100000>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-enabled;
				regulator-state-uv = <1100000>;
			};
		};

		rk816_ldo4_reg:regulator@7 {
			regulator-name= "rk816_ldo4";
			regulator-min-microvolt = <1100000>;
			regulator-max-microvolt = <1100000>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-enabled;
				regulator-state-uv = <1100000>;
			};
		};

		rk816_ldo5_reg: regulator@8 {
			regulator-name = "rk816_ldo5";
			regulator-min-microvolt = <3000000>;
			regulator-max-microvolt = <3000000>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-disabled;
				regulator-state-uv = <3000000>;
			};
		};

		rk816_ldo6_reg: regulator@9 {
			regulator-name = "rk816_ldo6";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			regulator-initial-state = <3>;
			regulator-always-on;
			regulator-state-mem {
				regulator-state-disabled;
				regulator-state-uv = <3300000>;
			};
		};
	};

	battery {
		ocv_table = <3400 3599 3671 3701 3728 3746 3762
			     3772 3781 3792 3816 3836 3866 3910
			     3942 3971 4002 4050 4088 4132 4183>;
		design_capacity = <4000>;
		design_qmax = <4100>;
		bat_res = <120>;
		max_input_current = <2000>;
		max_chrg_current = <1800>;
		max_chrg_voltage = <4200>;
		sleep_enter_current = <300>;
		sleep_exit_current = <300>;
		sleep_filter_current = <100>;
		power_off_thresd = <3400>;
		zero_algorithm_vol = <3850>;
		fb_temperature = <115>;
		energy_mode = <0>;
		max_soc_offset = <60>;
		monitor_sec = <3>;
		virtual_power = <0>;
		power_dc2otg = <1>;
		dc_det_adc = <1>;
	};
};

&pwm0 {
	status = "okay";
};

