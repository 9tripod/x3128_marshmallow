package org.xboot.test;

import java.io.IOException;
import org.xboot.test.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class FanActivity extends Activity {
	private Button mBtnFan;
	final private String mFan = "/sys/bus/platform/drivers/xgpio/xgpio_fan/state";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fan);
		mBtnFan = (Button) findViewById(R.id.btnFan);
	
		mBtnFan.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					startFan();
					break;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					stopFan();
					break;
				default:
					break;
				}
				return false;
			}
		});
	}

	void startFan() {
		try {
			DeviceIO.write(mFan, 1 + "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void stopFan() {
		try {
			DeviceIO.write(mFan, 0 + "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
