package com.android.rk;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.widget.Toast;
import android.util.Log;
/**
 * 本类用于处理发送文件是过滤如  gmail 等应用
 */

public class ShareIntent {
	private static String TAG= "ShareIntent";
    private static boolean DBG= false;
	static String [] filterList = {"com.google.android.gm"};
	
	/**
	 * 设置要过滤的关键字，针对包名进行过滤
	 */
	public static void SetFilter(String [] mList){
		filterList = mList;
	}
			
	/**
    *	一次发送多个文件
    *	文件类型一般使用：x-mixmedia/*
    */	
	public static void shareMultFile(Context mcontext,String mFileType,ArrayList<Uri> uris){
	    if(DBG){
		    for(Uri uri:uris)
                Log.d(TAG,"shareMultFile: uri="+uri);
    		Log.d(TAG,"shareMultFile:mFileType="+mFileType);
           }

		Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
		intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        intent.setType(mFileType);
		Intent intent1 = Intent.createChooser(intent,mcontext.getString(R.string.edit_bluetooth));
         intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS); 
         mcontext.startActivity(intent1);

	}
	
	/**
    *	一次发送一个文件，需要指文件类型
    *	 比如：image/* audio/* text/plain
    */	
	public static void shareFile(Context mcontext,String mFileType,Uri uri){
	    if(DBG)
			 Log.d(TAG,"shareFile:mFileType="+mFileType+",uri="+uri);
		 Intent intent = new Intent(Intent.ACTION_SEND);
       //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       
         intent.setType(mFileType);
         intent.putExtra(Intent.EXTRA_STREAM, uri);
         Intent intent1 = Intent.createChooser(intent,mcontext.getString(R.string.edit_bluetooth));
         intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS); 
         mcontext.startActivity(intent1);
	} 
		
    /**
    *	获取过滤后的app列表
    *	直接过滤包名
    */
	private static List<Intent> getShareApps(Context mcontext,String mFileType,String mSendIntentType){
	    Intent it =new Intent(mSendIntentType);
	    it.setType(mFileType);
	    List<ResolveInfo> sendActiviryInfo = mcontext.getPackageManager().queryIntentActivities(it,0);
	    
	    if(!sendActiviryInfo.isEmpty()){
	        List<Intent> targetedShareIntents =new ArrayList<Intent>();
	        for(ResolveInfo info : sendActiviryInfo){
	            Intent targeted =new Intent(mSendIntentType);
	            targeted.setType(mFileType);
	            ActivityInfo activityInfo = info.activityInfo;
	            //在此处过滤包名
	            if(containKeyword(activityInfo.packageName)){
	                continue;
	            }
	            targeted.setPackage(activityInfo.packageName);
	            targetedShareIntents.add(targeted);
	        }
	        return targetedShareIntents;
	    }
		return null;
	}
	
	/**
	 * 检查包名是否包含关键字
	 */
	private static boolean containKeyword(String packageName){
		for(String name : filterList){
			if(packageName.contains(name) && !("").equals(name)){
				return true;
			}
		}
		return false;
	}
}
